function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card shadow-lg p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        <p>${start}-${end}</p>
        </div>
      </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("response not good")
      } else {
        const data = await response.json();
        let count = 0;

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                console.log(details);
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts);
                const start = startDate.toLocaleDateString();
                const endDate = new Date(details.conference.ends);
                const end = endDate.toLocaleDateString();
                const location = details.conference.location.name;
                const html = createCard(title, description, pictureUrl, start, end, location);


                const columns = document.querySelectorAll('.col');
                const column = columns[count]
                column.innerHTML += html;
                count++;
                if (count === 3) {
                    count = 0;
                }
            }
        }


    }
    } catch (e) {
        console.log("there is an error:", e)
    }

  });
