import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [states, setStates] = useState([]);
    const [name, setName] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const [description, setDescription] = useState("");
    const [max_presentations, setMaxPresentation] = useState("");
    const [max_attendees, setMaxAttendee] = useState("");
    const [location, setLocation] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.start = start;
        data.name = name;
        data.end = end;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxAttendee('');
            setMaxPresentation('');
            setLocation('');
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxAttendeeChange = (event) => {
        const value = event.target.value;
        setMaxAttendee(value);
    }

    const handleMaxPresentationChange = (event) => {
        const value = event.target.value;
        setMaxPresentation(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setStates(data.states)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form id="create-location-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" onChange={handleNameChange} required type="text" name = "name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Start Date" onChange={handleStartChange} required type="date" name = "start_date" id="start_date" className="form-control"/>
                <label htmlFor="start_date">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="End Date" onChange={handleEndChange} required type="date" id="end_date" name = "end_date" className="form-control"/>
                <label htmlFor="end_date">End Date</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum Presentations" onChange={handleMaxPresentationChange} required type="number" id="maximum_presentations" name = "maximum_presentations" className="form-control"/>
                <label htmlFor="maximum_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum Attendees" onChange={handleMaxAttendeeChange} required type="number" id="maximum_attendees" name = "maximum_attendees" className="form-control"/>
                <label htmlFor="maximum_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <textarea placeholder="Description" onChange={handleDescriptionChange} required type="text" id="description" name = "description" rows = "5" className="form-control"></textarea>
                <label htmlFor="description"></label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Location" onChange={handleLocationChange} required type="text" id="location" name = "location" className="form-control"/>
                <label htmlFor="location">Location</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm;
